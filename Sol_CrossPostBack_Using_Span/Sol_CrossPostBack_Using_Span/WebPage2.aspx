﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage2.aspx.cs" Inherits="Sol_CrossPostBack_Using_Span.WebPage2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <span id="spanName1" runat="server" EnableViewState="true"></span><br><br>
            <span id="spanName2" runat="server" EnableViewState="true"></span><br><br>
            <span id="spanName3" runat="server" EnableViewState="true"></span><br><br>
            <span id="spanName4" runat="server" EnableViewState="true"></span><br><br>
            <span id="spanName5" runat="server" EnableViewState="true"></span><br><br>

        </div>
    </form>
</body>
</html>
